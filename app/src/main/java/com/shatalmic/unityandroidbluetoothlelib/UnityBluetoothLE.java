package com.shatalmic.unityandroidbluetoothlelib;

import java.io.UnsupportedEncodingException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.math.BigInteger;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;

import com.unity3d.player.UnityPlayer;
import android.app.Activity;
import android.content.ContextWrapper;
import java.lang.Runnable;

//@SuppressLint("NewApi")
public class UnityBluetoothLE {


	public static UnityBluetoothLE _instance;

	public static final String TAG = "UnityBluetoothLE";
	
	private static final String UNITY_SEND_MESSAGE_STRING_1 = "BluetoothLEReceiver";
	private static final String UNITY_SEND_MESSAGE_STRING_2 = "OnBluetoothMessage";
	private static final String FULL_BLUETOOTH_UUID_PREFIX = "0000";
	private static final String FULL_BLUETOOTH_UUID_SUFFIX = "-0000-1000-8000-00805F9B34FB";
	private static final String CHARACTERISTIC_UUID_PREFIX = "00000000";
	private static final String CHARACTERISTIC_UUID_SUFFIX = "-0000-1000-8000-00805f9b34fb";


	private static final String ERROR_BLE_UNAVAILABLE = "Error~Bluetooth Low Energy Not Available";
	private static final String INITIALIZED = "Initialized";
	private static final String DEINITIALIZED = "DeInitialized";
	private static final String ERROR_BLE_NOT_ENABLED = "Error~Bluetooth LE Not Enabled";
	private static final String DEVICE_DEFAULT_NAME = "No Name";
	private static final String DISCOVERED_PERIPHERAL = "DiscoveredPeripheral";
	private static final String CONNECTED_PERIPHERAL = "ConnectedPeripheral";
	private static final String DISCONNECTED_PERIPHERAL = "DisconnectedPeripheral";
	private static final String DISCOVERY_STARTED = "DiscoveryStarted";
	private static final String DISCOVERED_SERVICE = "DiscoveredService";
	private static final String DISCOVERED_CHARACTERISTIC = "DiscoveredCharacteristic";
	private static final String DISCOVERY_FINISHED = "DiscoveryFinished";
	private static final String ERROR_SERVICE_DISCOVERY = "Error~Service Discovery ";
	private static final String ERROR_RESPONSE_FAILED_TO_WRITE_CHARACTERISTIC = "Error~Response - failed to write characteristic: ";
	private static final String ERROR_DESCRIPTOR_WRITE_FAILED_CHARACTERISTIC_OR_UUID_BLANK = "Error~Descriptor Write Failed: characterstic or uuid blank";
	private static final String ERROR_DESCRIPTOR_WRITE_FAILED = "Error~Descriptor Write Failed: ";
	private static final String DELIM = "~";
	private static final String DELIM_REPLACE = "-";
	private static final String EMPTY = "";
	private static final String DID_UPDATE_VALUE_FOR_CHARACTERISTIC = "DidUpdateValueForCharacteristic";
	private static final String DID_UPDATE_NOTIFICATION_STATE_FOR_CHARACTERISTIC = "DidUpdateNotificationStateForCharacteristic";
	private static final String DID_WRITE_CHARACTERISTIC= "DidWriteCharacteristic";

	private static final String ERROR_FAILED_TO_READ_CHARACTERISTIC = "Error~Failed to read characteristic";
	private static final String ERROR_CHARACTERISTIC_NOT_FOUND_FOR_READ = "Error~Characteristic not found for Read";
	private static final String ERROR_NOT_A_VALID_CHARACTERISTIC_FOR_READ = "Error~Not a Valid Characteristic UUID for Read";
	private static final String ERROR_SERVICE_NOT_FOUND_FOR_READ = "Error~Service not found for Read";
	private static final String ERROR_NOT_A_VALID_SERVICE_FOR_READ = "Error~Not a Valid Service UUID for Read";

	private static final String ERROR_FAILED_TO_WRITE_CHARACTERISTIC = "Error~Failed to write characteristic";
	private static final String ERROR_CHARACTERISTIC_NOT_FOUND_FOR_WRITE = "Error~Characteristic not found for Write";
	private static final String ERROR_NOT_A_VALID_CHARACTERISTIC_FOR_WRITE = "Error~Not a Valid Characteristic UUID for Write";
	private static final String ERROR_SERVICE_NOT_FOUND_FOR_WRITE = "Error~Service not found for Write";
	private static final String ERROR_NOT_A_VALID_SERVICE_FOR_WRITE = "Error~Not a Valid Service UUID for Write";

	private static final String ERROR_FAILED_TO_WRITE_CHARACTERISTIC_DESCRIPTOR = "Error~Failed to write characteristic descriptor";
	private static final String ERROR_FAILED_TO_SET_NOTIFICATION_TYPE = "Error~Failed to set notification type";
	private static final String ERROR_FAILED_TO_GET_NOTIFICATION_DESCRIPTOR = "Error~Failed to get notification descriptor";
	private static final String ERROR_FAILED_TO_SET_CHARACTERISTIC_NOTIFICATION = "Error~Failed to set characteristic notification";
	private static final String ERROR_CHARACTERISTIC_NOT_FOUND_FOR_SUBSCRIBE = "Error~Characteristic not found for Subscribe";
	private static final String ERROR_NOT_A_VALID_CHARACTERISTIC_UUID_FOR_SUBSCRIBE = "Error~Not a Valid Characteristic UUID for Subscribe";
	private static final String ERROR_SERVICE_NOT_FOUND_FOR_SUBSCRIBE = "Error~Service not found for Subscribe";
	private static final String ERROR_NOT_A_VALID_SERVICE_UUID_FOR_SUBSCRIBE = "Error~Not a Valid Service UUID for Subscribe";



	public AtomicBoolean mRunning = new AtomicBoolean();

	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothLeScanner mBluetoothLeScanner;
	private ArrayList<UUID> uuidList = null;
	private Map<String, BluetoothDevice> deviceMap = null;
	private Map<String, BluetoothGatt> deviceGattMap = null;
	private Queue<byte[]> receivedData = null;
	private Queue<String> receivedDataCharacteristicUUID= null;


	private Context mContext = null;

	public static UnityBluetoothLE getInstance() {

		if (_instance == null)
			_instance = new UnityBluetoothLE();

		return _instance;
	}

	public static void UnitySend(String message) {

		UnityPlayer.UnitySendMessage(UNITY_SEND_MESSAGE_STRING_1, UNITY_SEND_MESSAGE_STRING_2, message);
	}

	public static void UnitySend(byte[] data, int length) {

		String message = Base64.encodeToString(Arrays.copyOfRange(data, 0, length), Base64.DEFAULT);
		UnityPlayer.UnitySendMessage(UNITY_SEND_MESSAGE_STRING_1, UNITY_SEND_MESSAGE_STRING_2, message);
	}

	private UUID getFullBluetoothLEUUID (String uuidString) {

		//androidBluetoothLog("getFullBluetoothLEUUID : "+uuidString);

		UUID uuid;

		if (uuidString.length() == 4)
			uuid = UUID.fromString(FULL_BLUETOOTH_UUID_PREFIX + uuidString + FULL_BLUETOOTH_UUID_SUFFIX);
		else
			uuid = UUID.fromString(uuidString);

		return uuid;
	}

	private UUID getUUIDFromString(String uuidString) {
		String s2 = uuidString.replace(DELIM_REPLACE, EMPTY);
		UUID uuid = new UUID(
				new BigInteger(s2.substring(0, 16), 16).longValue(),
				new BigInteger(s2.substring(16), 16).longValue());

		return uuid;
	}
/*
	private final BroadcastReceiver AdapterStateChangedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if ( BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
				int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
				boolean adapterEnabled = state == BluetoothAdapter.STATE_ON ? true : false;
				UnitySend("BluetoothEnabled~" + adapterEnabled);
			}
		}
	};
*/
	private final BroadcastReceiver ActionFoundReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			String action = intent.getAction();

			if (BluetoothDevice.ACTION_UUID.equals(action)) {

				//androidBluetoothLog("got action_uuid");

				if (uuidList != null) {

					BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					Parcelable[] uuidExtra = intent.getParcelableArrayExtra(BluetoothDevice.EXTRA_UUID);

					if (device != null && uuidExtra != null) {
						for (Parcelable uuidParcel : uuidExtra) {

							String uuid = uuidParcel.toString();

							//androidBluetoothLog("checking uuid " + uuid);

							if (uuidList.contains(getFullBluetoothLEUUID(uuid)))
								sendDiscoveredDevice (device, 0, null);
						}
					}
					/*else {

						if (device != null)
							androidBluetoothLog("device: " + device.getAddress());
						else
							androidBluetoothLog("device is null");

						if (uuidExtra != null)
							androidBluetoothLog("uuid count: " + uuidExtra.length);
						else
							androidBluetoothLog("uuidExtra is null");
					}*/
				}
			}
		} 
	};

	private Activity getActivity(Context context) {
		if (context == null) return null;
		if (context instanceof Activity) return (Activity) context;
		if (context instanceof ContextWrapper) return getActivity(((ContextWrapper)context).getBaseContext());
		return null;
	}

	// JW: prevent app from going into pause mode
	public void setFlagKeepScreenOn()
	{
		Log.d(TAG, "trying to set FLAG_KEEP_SCREEN_ON");

		getActivity(UnityPlayer.currentActivity).runOnUiThread(new Runnable() {
			@Override
			public void run() {
				getActivity(UnityPlayer.currentActivity).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			}
		});

		Log.d(TAG, "finished setting FLAG_KEEP_SCREEN_ON");
	}

	public void androidBluetoothInitialize(boolean asCentral, boolean asPeripheral) {

		//Log.d(TAG, "androidBluetoothInitialize");

		// Initializes Bluetooth adapter.
		mContext = UnityPlayer.currentActivity.getApplicationContext();
		receivedData = new ArrayDeque<>();
		receivedDataCharacteristicUUID = new ArrayDeque<>();

		// Use this check to determine whether BLE is supported on the device. Then
		// you can selectively disable BLE-related features.
		if (!mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
			UnitySend(ERROR_BLE_UNAVAILABLE);
		}
		else {

			uuidList = null;
			deviceMap = null;
			deviceGattMap = null;

			//Register the BroadcastReceiver
			IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
			filter.addAction(BluetoothDevice.ACTION_UUID);
			mContext.registerReceiver(ActionFoundReceiver, filter); // Don't forget to unregister during onDestroy

			//Register Bluetooth adapter listener
			//IntentFilter adapterFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
			//mContext.registerReceiver( AdapterStateChangedReceiver, adapterFilter);


			if (BluetoothAdapter.getDefaultAdapter() != null) {

				final BluetoothManager bluetoothManager =
						(BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);

				if (bluetoothManager != null) {

					mBluetoothAdapter = bluetoothManager.getAdapter();

					if (mBluetoothAdapter != null) {

						if (mBluetoothAdapter.isEnabled()) {

							if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

								mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
							}

							UnitySend(INITIALIZED);
						}
						else {
							UnitySend(ERROR_BLE_NOT_ENABLED);
						}
					}
					else
						onDestroy();
				}
				else
					onDestroy();
			}
			else
				UnitySend(ERROR_BLE_UNAVAILABLE);
		}
	}

	public void androidBluetoothLog(String message) {

		Log.i(TAG, message);
	}

	public void androidBluetoothDeInitialize() {

		//Log.d(TAG, "androidBluetoothDeInitialize");

		onDestroy();
		UnitySend(DEINITIALIZED);
	}

	public boolean androidIsBluetoothEnabled() {
		if(BluetoothAdapter.getDefaultAdapter() == null)
			return false;
		else
			return BluetoothAdapter.getDefaultAdapter().isEnabled();
	}

	public byte[] androidPollReceivedData() {
		return receivedData.poll();
	}
	public String androidPollReceivedDataCUUID() {
		return receivedDataCharacteristicUUID.poll();
	}

	public void androidBluetoothEnable(boolean enable) {

		//androidBluetoothLog("androidBluetoothEnable");
		/*
		if (mBluetoothAdapter == null) {

			UnitySend("Error~Plugin Initialize not called.");
		}
		else {

*/

			//if (enable)
				//not for apps just requesting bluetooth, but for settings interactions (https://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html)
				//mBluetoothAdapter.enable();
			//	mContext.sendBroadcast(changeBluetoothState);

		//	else
			//	mBluetoothAdapter.disable();
	//	}
	}

	public void onDestroy() {

		if (mContext != null) {

			mContext.unregisterReceiver(ActionFoundReceiver);
			uuidList = null;
			deviceMap = null;
			deviceGattMap = null;
		}
	}

	public class AdRecord {

		public int Length;
		public int Type;
		public byte[] Data;

		public AdRecord(int length, int type, byte[] data) {

			Length = length;
			Type = type;
			Data = data;
		}

		public String decodeRecord() {

			String decodedRecord = EMPTY;

			try {
				decodedRecord = new String(Data, "UTF-8");

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			return decodedRecord;
		}
	}

	public class AdRecordList {

		List<AdRecord> Records = new ArrayList<AdRecord>();

		public List<AdRecord> parseScanRecord(byte[] scanRecord) {

			int index = 0;
			while (index < scanRecord.length) {

				int length = scanRecord[index++];
				//Done once we run out of records
				if (length == 0) break;

				int type = scanRecord[index];
				//Done if our record isn't a valid type
				if (type == 0) break;

				byte[] data = Arrays.copyOfRange(scanRecord, index+1, index+length);

				Records.add(new AdRecord(length, type, data));

				//Advance
				index += length;
			}

			return Records;
		}

		public AdRecord getRecord(int type) {

			for (AdRecord record : Records) {

				if (record.Type == type)
					return record;
			}

			return null;
		}
	}

	private boolean _noManufacturerData = false;
	private int _recordType = 0xFF;

	private void sendDiscoveredDevice (BluetoothDevice device, int rssi, byte[] scanRecord) {

		String name = DEVICE_DEFAULT_NAME;
		if (device.getName() != null)
			name = device.getName();

		if (deviceMap == null)
			deviceMap = new HashMap<String, BluetoothDevice>();

		deviceMap.put(device.getAddress(), device);

		if (_noManufacturerData) {
			byte[] manufacturerBytes = null;

			if (scanRecord != null) {

				AdRecordList recordList = new AdRecordList();
				recordList.parseScanRecord(scanRecord);
				AdRecord record = recordList.getRecord(_recordType);

				if (record != null)
					manufacturerBytes = record.Data;
			}

			if (manufacturerBytes != null || rssi != 0) {
				if (manufacturerBytes == null)
					manufacturerBytes = new byte[] { 0x00 };

				UnitySend(DISCOVERED_PERIPHERAL + DELIM + device.getAddress() + DELIM + name + DELIM + Integer.valueOf(rssi) + DELIM + Base64.encodeToString(manufacturerBytes, Base64.DEFAULT));
			}
			else
				UnitySend(DISCOVERED_PERIPHERAL + DELIM + device.getAddress() + DELIM + name);
		}
		else {
			if (scanRecord != null) {

				AdRecordList recordList = new AdRecordList();
				recordList.parseScanRecord(scanRecord);
				AdRecord record = recordList.getRecord(-1);

				if (record != null)
					UnitySend(DISCOVERED_PERIPHERAL + DELIM  + device.getAddress() + DELIM + name + DELIM + Integer.valueOf(rssi) + DELIM + Base64.encodeToString(record.Data, Base64.DEFAULT));
				else
					UnitySend(DISCOVERED_PERIPHERAL + DELIM  + device.getAddress() + DELIM + name);
			}
			else {

				UnitySend(DISCOVERED_PERIPHERAL + DELIM  + device.getAddress() + DELIM + name);
			}
		}
	}

	// Device scan callback.


	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
		@Override
		public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

			sendDiscoveredDevice (device, rssi, scanRecord);
		};
	};
	/*
	// Device scan callback for >= SDK 21 (LolliPop)
	private ScanCallback mScanCallback = new ScanCallback() {

		@Override
        public void onScanResult(int callbackType, ScanResult result) {
            androidBluetoothLog("onScanResult");
            processResult(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
        	androidBluetoothLog("onBatchScanResults: " + results.size() + " results");
            for (ScanResult result : results) {
                processResult(result);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
        	androidBluetoothLog("LE Scan Failed: " + errorCode);
        }

        private void processResult(ScanResult result) {
        	androidBluetoothLog("New LE Device: " + result.getDevice().getName() + " @ " + result.getRssi());
	    	sendDiscoveredDevice (result.getDevice());
        }
	};
	*/
	public void androidBluetoothPause (boolean paused) {


	}

	private ScanCallback api21ScanCallback;

	@TargetApi(Build.VERSION_CODES.M)

	private void api21Scan(List<UUID> uuids) {

		List<ScanFilter> scanFilters = new ArrayList<ScanFilter>();

		if (uuids != null) {
			for (int i = 0; i < uuids.size(); ++i)
				scanFilters.add(new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(uuids.get(i).toString())).build());
		}

		ScanSettings settings = new ScanSettings.Builder().setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES).build();
		ScanCallback scanCallback = new ScanCallback() {
			@Override
			public void onScanResult(int callbackType, ScanResult result)
			{
				mLeScanCallback.onLeScan(result.getDevice(), result.getRssi(), result.getScanRecord().getBytes());
			}

			@Override
			public void onBatchScanResults(List<ScanResult> results)
			{
				for (ScanResult sr : results)
				{
					mLeScanCallback.onLeScan(sr.getDevice(), sr.getRssi(), sr.getScanRecord().getBytes());
				}
			}
		};

		api21ScanCallback = scanCallback;

		if (uuids != null)
			mBluetoothAdapter.getBluetoothLeScanner().startScan(scanFilters, settings, scanCallback);
		else
			mBluetoothAdapter.getBluetoothLeScanner().startScan(null, settings, scanCallback);
	}

	public void androidBluetoothScanForPeripheralsWithServices (String serviceUUIDsString, boolean rssiOnly, int recordType) {

		_noManufacturerData = rssiOnly;
		_recordType = recordType;

		if (mBluetoothAdapter != null) {

			ArrayList<UUID> uuidList = new ArrayList<UUID>();

			if (serviceUUIDsString != null) {
				if (serviceUUIDsString.contains("|")) {
					//androidBluetoothLog("serviceUUIDsString : " + serviceUUIDsString);

					String[] serviceUUIDs = serviceUUIDsString.split("\\|");
					if (serviceUUIDs != null && serviceUUIDs.length > 0) {

						for (int i = 0; i < serviceUUIDs.length; ++i) {
							//androidBluetoothLog("serviceUUID : " + serviceUUIDs[i]);
							if (serviceUUIDs[i] != null && serviceUUIDs[i].length() >= 4)
								uuidList.add(getFullBluetoothLEUUID(serviceUUIDs[i]));
						}
					}
				}
				else if (serviceUUIDsString.length() > 0)
					uuidList.add(getFullBluetoothLEUUID(serviceUUIDsString));
			}

			if (uuidList.size() > 0) {

				UUID[] uuids = new UUID[uuidList.size()];
				uuids = uuidList.toArray(uuids);

				/*for (int i = 0; i < uuids.length; ++i)
					androidBluetoothLog("Scanning for: " + uuids[i].toString());*/

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					//androidBluetoothLog("Using api21scan");
					api21Scan(uuidList);
				}
				else {
					//androidBluetoothLog("Using legacy scan");
					mBluetoothAdapter.startLeScan(uuids, mLeScanCallback);
				}
			}
			else {

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					//androidBluetoothLog("Using api21scan");
					api21Scan(uuidList);
				}
				else {
					//androidBluetoothLog("Using legacy scan");
					mBluetoothAdapter.startLeScan(mLeScanCallback);
				}
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	private void api21StopScan () {
		if (api21ScanCallback != null)
			mBluetoothAdapter.getBluetoothLeScanner().stopScan(api21ScanCallback);
	}

	public void androidBluetoothStopScan () {

		if (mBluetoothAdapter != null) {

			if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
				api21StopScan ();
			else
				mBluetoothAdapter.stopLeScan(mLeScanCallback);
		}
	}

	public void androidBluetoothRetrieveListOfPeripheralsWithServices (String serviceUUIDsString) {

		if (mBluetoothAdapter != null) {

			uuidList = new ArrayList<UUID>();

			if (serviceUUIDsString.contains("|")) {

				String[] serviceUUIDs = serviceUUIDsString.split("|");
				if (serviceUUIDs != null && serviceUUIDs.length > 0) {

					for (int i = 0; i < serviceUUIDs.length; ++i)
						uuidList.add(getFullBluetoothLEUUID(serviceUUIDs[i]));
				}
			}
			else if (serviceUUIDsString.length() > 0)
				uuidList.add(getFullBluetoothLEUUID(serviceUUIDsString));

			//androidBluetoothLog("getting bonded devices");

			UnityPlayer.currentActivity.runOnUiThread(new Runnable() {

				@Override
				public void run() {

					Set<BluetoothDevice> deviceSet = mBluetoothAdapter.getBondedDevices();
					if (deviceSet != null && deviceSet.size() > 0) {

						for (BluetoothDevice device : deviceSet) {

							if (device != null) {

								//androidBluetoothLog("got device " + device.getAddress());

								if (uuidList.size() > 0)
									device.fetchUuidsWithSdp();
								else
									sendDiscoveredDevice (device, 0, null);
							}
						}
					}
				}
			});
		}
	}

	// Various callback methods defined by the BLE API.
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {

			//androidBluetoothLog("onConnectionStateChange");

			String deviceAddress = gatt.getDevice().getAddress();

			if (newState == BluetoothProfile.STATE_CONNECTED) {

				if (deviceGattMap == null)
					deviceGattMap = new HashMap<String, BluetoothGatt>();

				deviceGattMap.put(deviceAddress, gatt);

				UnitySend(CONNECTED_PERIPHERAL + DELIM + deviceAddress);

				// start discovering services
				gatt.discoverServices();

			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {

				gatt.close();

				UnitySend(DISCONNECTED_PERIPHERAL + DELIM + deviceAddress);
			}
		}

		@Override
		// New services discovered
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS) {

				//androidBluetoothLog("Services Discovered");

				List<BluetoothGattService> services = gatt.getServices();
				if (services != null) {

					String deviceAddress = gatt.getDevice().getAddress();
					
					//report services
					UnitySend(DISCOVERY_STARTED + DELIM + deviceAddress);
					
					for (BluetoothGattService service : services) {

						UnitySend(DISCOVERED_SERVICE + DELIM + deviceAddress + DELIM + service.getUuid());

						List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
						for (BluetoothGattCharacteristic characteristic : characteristics) {

							UnitySend(DISCOVERED_CHARACTERISTIC + DELIM + deviceAddress + DELIM + service.getUuid() + DELIM + characteristic.getUuid());
						}
					}
					//report services
					UnitySend(DISCOVERY_FINISHED + DELIM + deviceAddress);

				}
				
				
				
			} else {

				androidBluetoothLog(ERROR_SERVICE_DISCOVERY + status);
			}
		}

		@Override
		// Result of a characteristic read operation
		public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS) {

				byte[] data = characteristic.getValue();
				if (data != null) {

					String deviceAddress = gatt.getDevice().getAddress();
					//String dataString = Base64.encodeToString(data, Base64.DEFAULT);
					receivedData.add(data);
					receivedDataCharacteristicUUID.add(characteristic.getUuid().toString());
					UnitySend(DID_UPDATE_VALUE_FOR_CHARACTERISTIC + DELIM + deviceAddress + DELIM + characteristic.getUuid() + DELIM);

				}
			}
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

			byte[] data = characteristic.getValue();
			if (data != null) {

				String deviceAddress = gatt.getDevice().getAddress();
				//String dataString = Base64.encodeToString(data, Base64.DEFAULT);
				receivedData.add(data);
				receivedDataCharacteristicUUID.add(characteristic.getUuid().toString());
				UnitySend(DID_UPDATE_VALUE_FOR_CHARACTERISTIC + DELIM + deviceAddress + DELIM + characteristic.getUuid() + DELIM);
			}
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS)
				UnitySend(DID_WRITE_CHARACTERISTIC + DELIM + characteristic.getUuid());
			else
				UnitySend(ERROR_RESPONSE_FAILED_TO_WRITE_CHARACTERISTIC + status);
		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS) {

				//androidBluetoothLog("onDescriptorWrite Success");
				if (descriptor != null && descriptor.getCharacteristic() != null && descriptor.getCharacteristic().getUuid() != null) {
					String deviceAddress = gatt.getDevice().getAddress();
					UnitySend(DID_UPDATE_NOTIFICATION_STATE_FOR_CHARACTERISTIC + DELIM + deviceAddress + DELIM + descriptor.getCharacteristic().getUuid());
				}
				else
					UnitySend(ERROR_DESCRIPTOR_WRITE_FAILED_CHARACTERISTIC_OR_UUID_BLANK);
			}
			else
				UnitySend(ERROR_DESCRIPTOR_WRITE_FAILED + status);
		}
	};

	public void androidBluetoothConnectToPeripheral(String name) {

		if (mBluetoothAdapter != null && deviceMap != null && mContext != null) {

			final BluetoothDevice device = deviceMap.get(name);
			if (device != null) {

				UnityPlayer.currentActivity.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						device.connectGatt(mContext, false, mGattCallback);
					}
				});
			}
		}
	}

	public void androidBluetoothDisconnectPeripheral(String name) {

		if (mBluetoothAdapter != null && deviceGattMap != null && mContext != null) {

			BluetoothGatt gatt = deviceGattMap.get(name);
			if (gatt != null)
				gatt.disconnect();
		}
	}

	protected UUID getUUID(String uuidString) {

		UUID uuid = null;

		if (uuidString.length() == 36) {

			uuid = UUID.fromString(uuidString);
		}
		else if (uuidString.length() <= 8) {

			StringBuilder newString = new StringBuilder();
			newString.append(CHARACTERISTIC_UUID_PREFIX, 0, 8 - uuidString.length());
			newString.append(uuidString);
			newString.append(CHARACTERISTIC_UUID_SUFFIX);

			uuid = UUID.fromString(newString.toString());
		}

		return uuid;
	}

	public void androidReadCharacteristic(String name, String serviceString, String characteristicString) {

		if (mBluetoothAdapter != null && deviceGattMap != null && mContext != null) {

			BluetoothGatt gatt = deviceGattMap.get(name);
			if (gatt != null) {

				UUID serviceUUID = getUUID(serviceString);
				if (serviceUUID != null) {

					BluetoothGattService service = gatt.getService(serviceUUID);
					if (service != null) {

						UUID characteristicUUID = getUUID(characteristicString);
						if (characteristicUUID != null) {

							BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUUID);
							if (characteristic != null) {

								if (!gatt.readCharacteristic(characteristic))
									UnitySend(ERROR_FAILED_TO_READ_CHARACTERISTIC);
							}
							else
								UnitySend(ERROR_CHARACTERISTIC_NOT_FOUND_FOR_READ);
						}
						else
							UnitySend(ERROR_NOT_A_VALID_CHARACTERISTIC_FOR_READ);
					}
					else
						UnitySend(ERROR_SERVICE_NOT_FOUND_FOR_READ);
				}
				else
					UnitySend(ERROR_NOT_A_VALID_SERVICE_FOR_READ);
			}
		}
	}

	public void androidWriteCharacteristic(String name, String serviceString, String characteristicString, byte[] data, int length, boolean withResponse) {

		if (mBluetoothAdapter != null && deviceGattMap != null && mContext != null) {

			BluetoothGatt gatt = deviceGattMap.get(name);
			if (gatt != null) {

				UUID serviceUUID = getUUID(serviceString);
				if (serviceUUID != null) {

					BluetoothGattService service = gatt.getService(serviceUUID);
					if (service != null) {

						UUID characteristicUUID = getUUID(characteristicString);
						if (characteristicUUID != null) {

							BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUUID);
							if (characteristic != null) {

								//androidBluetoothLog("write characteristic");
								characteristic.setValue(data);
								characteristic.setWriteType(withResponse ? BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT : BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
								if (!gatt.writeCharacteristic(characteristic))
									UnitySend(ERROR_FAILED_TO_WRITE_CHARACTERISTIC);
							}
							else
								UnitySend(ERROR_CHARACTERISTIC_NOT_FOUND_FOR_WRITE);
						}
						else
							UnitySend(ERROR_NOT_A_VALID_CHARACTERISTIC_FOR_WRITE);
					}
					else
						UnitySend(ERROR_SERVICE_NOT_FOUND_FOR_WRITE);
				}
				else
					UnitySend(ERROR_NOT_A_VALID_SERVICE_FOR_WRITE);
			}
		}
	}

	protected static final UUID CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

	protected void androidSetCharacteristicNotification(String name, String serviceString, String characteristicString, boolean enabled) {

		if (mBluetoothAdapter != null && deviceGattMap != null && mContext != null) {

			BluetoothGatt gatt = deviceGattMap.get(name);
			if (gatt != null) {

				UUID serviceUUID = getUUID(serviceString);
				if (serviceUUID != null) {

					BluetoothGattService service = gatt.getService(serviceUUID);
					if (service != null) {

						UUID characteristicUUID = getUUID(characteristicString);
						if (characteristicUUID != null) {

							BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUUID);
							if (characteristic != null) {

								if (gatt.setCharacteristicNotification(characteristic, true)) {

									BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID);
									if (descriptor != null) {

										int characteristicProperties = characteristic.getProperties();
										byte[] valueToSend = BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE;
										if (enabled) {

											if ((characteristicProperties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) == BluetoothGattCharacteristic.PROPERTY_NOTIFY)
												valueToSend = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;
											else if ((characteristicProperties & BluetoothGattCharacteristic.PROPERTY_INDICATE) == BluetoothGattCharacteristic.PROPERTY_INDICATE)
												valueToSend = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE;
										}

										if (valueToSend.length > 0) {
											//androidBluetoothLog("Notification Description: " + valueToSend[0]);
											descriptor.setValue(valueToSend);
											if (!gatt.writeDescriptor(descriptor))
												UnitySend(ERROR_FAILED_TO_WRITE_CHARACTERISTIC_DESCRIPTOR);
											//else
											//	androidBluetoothLog("Notification setup succeeded");
										}
										else
											UnitySend(ERROR_FAILED_TO_SET_NOTIFICATION_TYPE);
									}
									else
										UnitySend(ERROR_FAILED_TO_GET_NOTIFICATION_DESCRIPTOR);
								}
								else
									UnitySend(ERROR_FAILED_TO_SET_CHARACTERISTIC_NOTIFICATION);
							}
							else
								UnitySend(ERROR_CHARACTERISTIC_NOT_FOUND_FOR_SUBSCRIBE);
						}
						else
							UnitySend(ERROR_NOT_A_VALID_CHARACTERISTIC_UUID_FOR_SUBSCRIBE);
					}
					else
						UnitySend(ERROR_SERVICE_NOT_FOUND_FOR_SUBSCRIBE);
				}
				else
					UnitySend(ERROR_NOT_A_VALID_SERVICE_UUID_FOR_SUBSCRIBE);
			}
		}
	}

	public void androidSubscribeCharacteristic(String name, String serviceString, String characteristicString) {

		//androidBluetoothLog("subscribe characteristic");
		androidSetCharacteristicNotification(name, serviceString, characteristicString, true);
	}

	public void androidUnsubscribeCharacteristic(String name, String serviceString, String characteristicString) {

		//androidBluetoothLog("unsubscribe characteristic");
		androidSetCharacteristicNotification(name, serviceString, characteristicString, false);
	}

	public void androidBluetoothDisconnectAll() {

		if (mBluetoothAdapter != null && deviceGattMap != null
				&& mContext != null) {
			for (String key : deviceGattMap.keySet()) {
				BluetoothGatt gatt = deviceGattMap.get(key);
				if (gatt != null)
					gatt.disconnect();
			}
		}
	}
}
