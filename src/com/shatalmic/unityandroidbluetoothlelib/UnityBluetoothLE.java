package com.shatalmic.unityandroidbluetoothlelib;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.math.BigInteger;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

//@SuppressLint("NewApi")
public class UnityBluetoothLE {

	public static UnityBluetoothLE _instance;

	public static final String TAG = "UnityBluetoothLE";
	
	

	public AtomicBoolean mRunning = new AtomicBoolean();

	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothLeScanner mBluetoothLeScanner;
	private ArrayList<UUID> uuidList = null;
	private Map<String, BluetoothDevice> deviceMap = null;
	private Map<String, BluetoothGatt> deviceGattMap = null;

	private Context mContext = null;

	public static UnityBluetoothLE getInstance() {

		if (_instance == null)
			_instance = new UnityBluetoothLE();

		return _instance;
	}

	public static void UnitySend(String message) {

		UnityPlayer.UnitySendMessage("BluetoothLEReceiver", "OnBluetoothMessage", message);
	}

	public static void UnitySend(byte[] data, int length) {

		String message = Base64.encodeToString(Arrays.copyOfRange(data, 0, length), Base64.DEFAULT);
		UnityPlayer.UnitySendMessage("BluetoothLEReceiver", "OnBluetoothData", message);
	}

	private UUID getFullBluetoothLEUUID (String uuidString) {

		androidBluetoothLog("getFullBluetoothLEUUID : "+uuidString);

		UUID uuid;

		if (uuidString.length() == 4)
			uuid = UUID.fromString("0000" + uuidString + "-0000-1000-8000-00805F9B34FB");
		else
			uuid = UUID.fromString(uuidString);

		return uuid;
	}

	private UUID getUUIDFromString(String uuidString) {
		String s2 = uuidString.replace("-", "");
		UUID uuid = new UUID(
				new BigInteger(s2.substring(0, 16), 16).longValue(),
				new BigInteger(s2.substring(16), 16).longValue());

		return uuid;
	}

	private final BroadcastReceiver ActionFoundReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			String action = intent.getAction();

			if (BluetoothDevice.ACTION_UUID.equals(action)) {

				androidBluetoothLog("got action_uuid");

				if (uuidList != null) {

					BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					Parcelable[] uuidExtra = intent.getParcelableArrayExtra(BluetoothDevice.EXTRA_UUID);

					if (device != null && uuidExtra != null) {
						for (Parcelable uuidParcel : uuidExtra) {

							String uuid = uuidParcel.toString();

							androidBluetoothLog("checking uuid " + uuid);

							if (uuidList.contains(getFullBluetoothLEUUID(uuid)))
								sendDiscoveredDevice (device, 0, null);
						}
					}
					else {

						if (device != null)
							androidBluetoothLog("device: " + device.getAddress());
						else
							androidBluetoothLog("device is null");

						if (uuidExtra != null)
							androidBluetoothLog("uuid count: " + uuidExtra.length);
						else
							androidBluetoothLog("uuidExtra is null");
					}
				}
			}
		} 
	};

	public void androidBluetoothInitialize(boolean asCentral, boolean asPeripheral) {

		Log.d(TAG, "androidBluetoothInitialize");

		// Initializes Bluetooth adapter.
		mContext = UnityPlayer.currentActivity.getApplicationContext();

		// Use this check to determine whether BLE is supported on the device. Then
		// you can selectively disable BLE-related features.
		if (!mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
			UnitySend("Error~Bluetooth Low Energy Not Available");
		}
		else {

			uuidList = null;
			deviceMap = null;
			deviceGattMap = null;

			//Register the BroadcastReceiver
			IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
			filter.addAction(BluetoothDevice.ACTION_UUID);
			mContext.registerReceiver(ActionFoundReceiver, filter); // Don't forget to unregister during onDestroy

			if (BluetoothAdapter.getDefaultAdapter() != null) {

				final BluetoothManager bluetoothManager =
						(BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);

				if (bluetoothManager != null) {

					mBluetoothAdapter = bluetoothManager.getAdapter();

					if (mBluetoothAdapter != null) {

						if (mBluetoothAdapter.isEnabled()) {

							if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

								mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
							}

							UnitySend("Initialized");
						}
						else
							UnitySend("Error~Bluetooth LE Not Enabled");
					}
					else
						onDestroy();
				}
				else
					onDestroy();
			}
			else
				UnitySend("Error~Bluetooth LE Not Available");
		}
	}

	public void androidBluetoothLog(String message) {

		Log.i(TAG, message);
	}

	public void androidBluetoothDeInitialize() {

		Log.d(TAG, "androidBluetoothDeInitialize");

		onDestroy();
		UnitySend("DeInitialized");
	}

	public void androidBluetoothEnable(boolean enable) {

		androidBluetoothLog("androidBluetoothEnable");

		if (mBluetoothAdapter == null) {

			UnitySend("Error~Plugin Initialize not called.");
		}
		else {

			if (enable)
				mBluetoothAdapter.enable();
			else
				mBluetoothAdapter.disable();
		}
	}

	public void onDestroy() {

		if (mContext != null) {

			mContext.unregisterReceiver(ActionFoundReceiver);
			uuidList = null;
			deviceMap = null;
			deviceGattMap = null;
		}
	}

	public class AdRecord {

		public int Length;
		public int Type;
		public byte[] Data;

		public AdRecord(int length, int type, byte[] data) {

			Length = length;
			Type = type;
			Data = data;
		}

		public String decodeRecord() {

			String decodedRecord = "";

			try {
				decodedRecord = new String(Data, "UTF-8");

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			return decodedRecord;
		}
	}

	public class AdRecordList {

		List<AdRecord> Records = new ArrayList<AdRecord>();

		public List<AdRecord> parseScanRecord(byte[] scanRecord) {

			int index = 0;
			while (index < scanRecord.length) {

				int length = scanRecord[index++];
				//Done once we run out of records
				if (length == 0) break;

				int type = scanRecord[index];
				//Done if our record isn't a valid type
				if (type == 0) break;

				byte[] data = Arrays.copyOfRange(scanRecord, index+1, index+length);

				Records.add(new AdRecord(length, type, data));

				//Advance
				index += length;
			}

			return Records;
		}

		public AdRecord getRecord(int type) {

			for (AdRecord record : Records) {

				if (record.Type == type)
					return record;
			}

			return null;
		}
	}

	private boolean _noManufacturerData = false;
	private int _recordType = 0xFF;

	private void sendDiscoveredDevice (BluetoothDevice device, int rssi, byte[] scanRecord) {

		String name = "No Name";
		if (device.getName() != null)
			name = device.getName();

		if (deviceMap == null)
			deviceMap = new HashMap<String, BluetoothDevice>();

		deviceMap.put(device.getAddress(), device);

		if (_noManufacturerData) {
			byte[] manufacturerBytes = null;

			if (scanRecord != null) {

				AdRecordList recordList = new AdRecordList();
				recordList.parseScanRecord(scanRecord);
				AdRecord record = recordList.getRecord(_recordType);

				if (record != null)
					manufacturerBytes = record.Data;
			}

			if (manufacturerBytes != null || rssi != 0) {
				if (manufacturerBytes == null)
					manufacturerBytes = new byte[] { 0x00 };

				UnitySend("DiscoveredPeripheral~" + device.getAddress() + "~" + name + "~" + Integer.valueOf(rssi) + "~" + Base64.encodeToString(manufacturerBytes, Base64.DEFAULT));
			}
			else
				UnitySend("DiscoveredPeripheral~" + device.getAddress() + "~" + name);
		}
		else {
			if (scanRecord != null) {

				AdRecordList recordList = new AdRecordList();
				recordList.parseScanRecord(scanRecord);
				AdRecord record = recordList.getRecord(-1);

				if (record != null)
					UnitySend("DiscoveredPeripheral~" + device.getAddress() + "~" + name + "~" + Integer.valueOf(rssi) + "~" + Base64.encodeToString(record.Data, Base64.DEFAULT));
				else
					UnitySend("DiscoveredPeripheral~" + device.getAddress() + "~" + name);
			}
			else {

				UnitySend("DiscoveredPeripheral~" + device.getAddress() + "~" + name);
			}
		}
	}

	// Device scan callback.


	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
		@Override
		public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

			sendDiscoveredDevice (device, rssi, scanRecord);
		};
	};
	/*
	// Device scan callback for >= SDK 21 (LolliPop)
	private ScanCallback mScanCallback = new ScanCallback() {

		@Override
        public void onScanResult(int callbackType, ScanResult result) {
            androidBluetoothLog("onScanResult");
            processResult(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
        	androidBluetoothLog("onBatchScanResults: " + results.size() + " results");
            for (ScanResult result : results) {
                processResult(result);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
        	androidBluetoothLog("LE Scan Failed: " + errorCode);
        }

        private void processResult(ScanResult result) {
        	androidBluetoothLog("New LE Device: " + result.getDevice().getName() + " @ " + result.getRssi());
	    	sendDiscoveredDevice (result.getDevice());
        }
	};
	*/
	public void androidBluetoothPause (boolean paused) {


	}

	private ScanCallback api21ScanCallback;

	@TargetApi(Build.VERSION_CODES.M)

	private void api21Scan(List<UUID> uuids) {

		List<ScanFilter> scanFilters = new ArrayList<ScanFilter>();

		if (uuids != null) {
			for (int i = 0; i < uuids.size(); ++i)
				scanFilters.add(new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(uuids.get(i).toString())).build());
		}

		ScanSettings settings = new ScanSettings.Builder().setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES).build();
		ScanCallback scanCallback = new ScanCallback() {
			@Override
			public void onScanResult(int callbackType, ScanResult result)
			{
				mLeScanCallback.onLeScan(result.getDevice(), result.getRssi(), result.getScanRecord().getBytes());
			}

			@Override
			public void onBatchScanResults(List<ScanResult> results)
			{
				for (ScanResult sr : results)
				{
					mLeScanCallback.onLeScan(sr.getDevice(), sr.getRssi(), sr.getScanRecord().getBytes());
				}
			}
		};

		api21ScanCallback = scanCallback;

		if (uuids != null)
			mBluetoothAdapter.getBluetoothLeScanner().startScan(scanFilters, settings, scanCallback);
		else
			mBluetoothAdapter.getBluetoothLeScanner().startScan(null, settings, scanCallback);
	}

	public void androidBluetoothScanForPeripheralsWithServices (String serviceUUIDsString, boolean rssiOnly, int recordType) {

		_noManufacturerData = rssiOnly;
		_recordType = recordType;

		if (mBluetoothAdapter != null) {

			ArrayList<UUID> uuidList = new ArrayList<UUID>();

			if (serviceUUIDsString != null) {
				if (serviceUUIDsString.contains("|")) {
					androidBluetoothLog("serviceUUIDsString : " + serviceUUIDsString);

					String[] serviceUUIDs = serviceUUIDsString.split("\\|");
					if (serviceUUIDs != null && serviceUUIDs.length > 0) {

						for (int i = 0; i < serviceUUIDs.length; ++i) {
							androidBluetoothLog("serviceUUID : " + serviceUUIDs[i]);
							if (serviceUUIDs[i] != null && serviceUUIDs[i].length() >= 4)
								uuidList.add(getFullBluetoothLEUUID(serviceUUIDs[i]));
						}
					}
				}
				else if (serviceUUIDsString.length() > 0)
					uuidList.add(getFullBluetoothLEUUID(serviceUUIDsString));
			}

			if (uuidList.size() > 0) {

				UUID[] uuids = new UUID[uuidList.size()];
				uuids = uuidList.toArray(uuids);

				for (int i = 0; i < uuids.length; ++i)
					androidBluetoothLog("Scanning for: " + uuids[i].toString());

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					androidBluetoothLog("Using api21scan");
					api21Scan(uuidList);
				}
				else {
					androidBluetoothLog("Using legacy scan");
					mBluetoothAdapter.startLeScan(uuids, mLeScanCallback);
				}
			}
			else {

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					androidBluetoothLog("Using api21scan");
					api21Scan(uuidList);
				}
				else {
					androidBluetoothLog("Using legacy scan");
					mBluetoothAdapter.startLeScan(mLeScanCallback);
				}
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	private void api21StopScan () {
		if (api21ScanCallback != null)
			mBluetoothAdapter.getBluetoothLeScanner().stopScan(api21ScanCallback);
	}

	public void androidBluetoothStopScan () {

		if (mBluetoothAdapter != null) {

			if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
				api21StopScan ();
			else
				mBluetoothAdapter.stopLeScan(mLeScanCallback);
		}
	}

	public void androidBluetoothRetrieveListOfPeripheralsWithServices (String serviceUUIDsString) {

		if (mBluetoothAdapter != null) {

			uuidList = new ArrayList<UUID>();

			if (serviceUUIDsString.contains("|")) {

				String[] serviceUUIDs = serviceUUIDsString.split("|");
				if (serviceUUIDs != null && serviceUUIDs.length > 0) {

					for (int i = 0; i < serviceUUIDs.length; ++i)
						uuidList.add(getFullBluetoothLEUUID(serviceUUIDs[i]));
				}
			}
			else if (serviceUUIDsString.length() > 0)
				uuidList.add(getFullBluetoothLEUUID(serviceUUIDsString));

			androidBluetoothLog("getting bonded devices");

			UnityPlayer.currentActivity.runOnUiThread(new Runnable() {

				@Override
				public void run() {

					Set<BluetoothDevice> deviceSet = mBluetoothAdapter.getBondedDevices();
					if (deviceSet != null && deviceSet.size() > 0) {

						for (BluetoothDevice device : deviceSet) {

							if (device != null) {

								androidBluetoothLog("got device " + device.getAddress());

								if (uuidList.size() > 0)
									device.fetchUuidsWithSdp();
								else
									sendDiscoveredDevice (device, 0, null);
							}
						}
					}
				}
			});
		}
	}

	// Various callback methods defined by the BLE API.
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {

			androidBluetoothLog("onConnectionStateChange");

			String deviceAddress = gatt.getDevice().getAddress();

			if (newState == BluetoothProfile.STATE_CONNECTED) {

				if (deviceGattMap == null)
					deviceGattMap = new HashMap<String, BluetoothGatt>();

				deviceGattMap.put(deviceAddress, gatt);

				UnitySend("ConnectedPeripheral~" + deviceAddress);

				// start discovering services
				gatt.discoverServices();

			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {

				gatt.close();

				UnitySend("DisconnectedPeripheral~" + deviceAddress);
			}
		}

		@Override
		// New services discovered
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS) {

				androidBluetoothLog("Services Discovered");

				List<BluetoothGattService> services = gatt.getServices();
				if (services != null) {

					String deviceAddress = gatt.getDevice().getAddress();
					
					//report services
					UnitySend("DiscoveryStarted~" + deviceAddress);
					
					for (BluetoothGattService service : services) {

						UnitySend("DiscoveredService~" + deviceAddress + "~" + service.getUuid());

						List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
						for (BluetoothGattCharacteristic characteristic : characteristics) {

							UnitySend("DiscoveredCharacteristic~" + deviceAddress + "~" + service.getUuid() + "~" + characteristic.getUuid());
						}
					}
					//report services
					UnitySend("DiscoveryFinished~" + deviceAddress);

				}
				
				
				
			} else {

				androidBluetoothLog("Error~Service Discovery " + status);
			}
		}

		@Override
		// Result of a characteristic read operation
		public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS) {

				byte[] data = characteristic.getValue();
				if (data != null) {

					String deviceAddress = gatt.getDevice().getAddress();
					String dataString = Base64.encodeToString(data, Base64.DEFAULT);
					UnitySend("DidUpdateValueForCharacteristic~" + deviceAddress + "~" + characteristic.getUuid() + "~" + dataString);
				}
			}
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

			byte[] data = characteristic.getValue();
			if (data != null) {

				String deviceAddress = gatt.getDevice().getAddress();
				String dataString = Base64.encodeToString(data, Base64.DEFAULT);
				UnitySend("DidUpdateValueForCharacteristic~" + deviceAddress + "~" + characteristic.getUuid() + "~" + dataString);
			}
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS)
				UnitySend("DidWriteCharacteristic~" + characteristic.getUuid());
			else
				UnitySend("Error~Response - failed to write characteristic: " + status);
		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS) {

				androidBluetoothLog("onDescriptorWrite Success");
				if (descriptor != null && descriptor.getCharacteristic() != null && descriptor.getCharacteristic().getUuid() != null) {
					String deviceAddress = gatt.getDevice().getAddress();
					UnitySend("DidUpdateNotificationStateForCharacteristic~" + deviceAddress + "~" + descriptor.getCharacteristic().getUuid());
				}
				else
					UnitySend("Error~Descriptor Write Failed: characterstic or uuid blank");
			}
			else
				UnitySend("Error~Descriptor Write Failed: " + status);
		}
	};

	public void androidBluetoothConnectToPeripheral(String name) {

		if (mBluetoothAdapter != null && deviceMap != null && mContext != null) {

			final BluetoothDevice device = deviceMap.get(name);
			if (device != null) {

				UnityPlayer.currentActivity.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						device.connectGatt(mContext, false, mGattCallback);
					}
				});
			}
		}
	}

	public void androidBluetoothDisconnectPeripheral(String name) {

		if (mBluetoothAdapter != null && deviceGattMap != null && mContext != null) {

			BluetoothGatt gatt = deviceGattMap.get(name);
			if (gatt != null)
				gatt.disconnect();
		}
	}

	protected UUID getUUID(String uuidString) {

		UUID uuid = null;

		if (uuidString.length() == 36) {

			uuid = UUID.fromString(uuidString);
		}
		else if (uuidString.length() <= 8) {

			StringBuilder newString = new StringBuilder();
			newString.append("00000000", 0, 8 - uuidString.length());
			newString.append(uuidString);
			newString.append("-0000-1000-8000-00805f9b34fb");

			uuid = UUID.fromString(newString.toString());
		}

		return uuid;
	}

	public void androidReadCharacteristic(String name, String serviceString, String characteristicString) {

		if (mBluetoothAdapter != null && deviceGattMap != null && mContext != null) {

			BluetoothGatt gatt = deviceGattMap.get(name);
			if (gatt != null) {

				UUID serviceUUID = getUUID(serviceString);
				if (serviceUUID != null) {

					BluetoothGattService service = gatt.getService(serviceUUID);
					if (service != null) {

						UUID characteristicUUID = getUUID(characteristicString);
						if (characteristicUUID != null) {

							BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUUID);
							if (characteristic != null) {

								if (!gatt.readCharacteristic(characteristic))
									UnitySend("Error~Failed to read characteristic");
							}
							else
								UnitySend("Error~Characteristic not found for Read");
						}
						else
							UnitySend("Error~Not a Valid Characteristic UUID for Read");
					}
					else
						UnitySend("Error~Service not found for Read");
				}
				else
					UnitySend("Error~Not a Valid Service UUID for Read");
			}
		}
	}

	public void androidWriteCharacteristic(String name, String serviceString, String characteristicString, byte[] data, int length, boolean withResponse) {

		if (mBluetoothAdapter != null && deviceGattMap != null && mContext != null) {

			BluetoothGatt gatt = deviceGattMap.get(name);
			if (gatt != null) {

				UUID serviceUUID = getUUID(serviceString);
				if (serviceUUID != null) {

					BluetoothGattService service = gatt.getService(serviceUUID);
					if (service != null) {

						UUID characteristicUUID = getUUID(characteristicString);
						if (characteristicUUID != null) {

							BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUUID);
							if (characteristic != null) {

								androidBluetoothLog("write characteristic");
								characteristic.setValue(data);
								characteristic.setWriteType(withResponse ? BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT : BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
								if (!gatt.writeCharacteristic(characteristic))
									UnitySend("Error~Failed to write characteristic");
							}
							else
								UnitySend("Error~Characteristic not found for Write");
						}
						else
							UnitySend("Error~Not a Valid Characteristic UUID for Write");
					}
					else
						UnitySend("Error~Service not found for Write");
				}
				else
					UnitySend("Error~Not a Valid Service UUID for Write");
			}
		}
	}

	protected static final UUID CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

	protected void androidSetCharacteristicNotification(String name, String serviceString, String characteristicString, boolean enabled) {

		if (mBluetoothAdapter != null && deviceGattMap != null && mContext != null) {

			BluetoothGatt gatt = deviceGattMap.get(name);
			if (gatt != null) {

				UUID serviceUUID = getUUID(serviceString);
				if (serviceUUID != null) {

					BluetoothGattService service = gatt.getService(serviceUUID);
					if (service != null) {

						UUID characteristicUUID = getUUID(characteristicString);
						if (characteristicUUID != null) {

							BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUUID);
							if (characteristic != null) {

								if (gatt.setCharacteristicNotification(characteristic, true)) {

									BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID);
									if (descriptor != null) {

										int characteristicProperties = characteristic.getProperties();
										byte[] valueToSend = BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE;
										if (enabled) {

											if ((characteristicProperties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) == BluetoothGattCharacteristic.PROPERTY_NOTIFY)
												valueToSend = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;
											else if ((characteristicProperties & BluetoothGattCharacteristic.PROPERTY_INDICATE) == BluetoothGattCharacteristic.PROPERTY_INDICATE)
												valueToSend = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE;
										}

										if (valueToSend.length > 0) {
											androidBluetoothLog("Notification Description: " + valueToSend[0]);
											descriptor.setValue(valueToSend);
											if (!gatt.writeDescriptor(descriptor))
												UnitySend("Error~Failed to write characteristic descriptor");
											else
												androidBluetoothLog("Notification setup succeeded");
										}
										else
											UnitySend("Error~Failed to set notification type");
									}
									else
										UnitySend("Error~Failed to get notification descriptor");
								}
								else
									UnitySend("Error~Failed to set characteristic notification");
							}
							else
								UnitySend("Error~Characteristic not found for Subscribe");
						}
						else
							UnitySend("Error~Not a Valid Characteristic UUID for Subscribe");
					}
					else
						UnitySend("Error~Service not found for Subscribe");
				}
				else
					UnitySend("Error~Not a Valid Service UUID for Subscribe");
			}
		}
	}

	public void androidSubscribeCharacteristic(String name, String serviceString, String characteristicString) {

		androidBluetoothLog("subscribe characteristic");
		androidSetCharacteristicNotification(name, serviceString, characteristicString, true);
	}

	public void androidUnsubscribeCharacteristic(String name, String serviceString, String characteristicString) {

		androidBluetoothLog("unsubscribe characteristic");
		androidSetCharacteristicNotification(name, serviceString, characteristicString, false);
	}

	public void androidBluetoothDisconnectAll() {

		if (mBluetoothAdapter != null && deviceGattMap != null
				&& mContext != null) {
			for (String key : deviceGattMap.keySet()) {
				BluetoothGatt gatt = deviceGattMap.get(key);
				if (gatt != null)
					gatt.disconnect();
			}
		}
	}
}
